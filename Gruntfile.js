module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    sass: {
      dev: {
        files: {
          'public/css/app.css': 'app/scss/app.scss'
        }
      }
    },

    concat: {
      options: {
        separator: ';',
        sourceMap: true
      },
      dist: {
        src: [
          //Libraries
          'bower_components/angular/angular.js',
          'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
          //Main application file
          'app/app.js',
          //Router
          'app/router.js',
          //Components
          'app/components/*.js',
          //Controllers
          'app/controllers/*.js',
          //Directives
          'app/directives/*.js',
          //Services
          'app/services/*.js'
        ],
        dest: 'public/js/app.js'
      }
    },

    copy: {
      main: {
        files: [
          {
            expand: true, 
            cwd: 'bower_components/bootstrap-sass/assets/',
            src: ['fonts/**'], 
            dest: 'public'
          },
        ],
      },
    },

    watch: {
      sass: {
        files: 'app/scss/**/*.scss',
        tasks: ['sass']
      },
      js: {
        files: ['app/**/*.js'],
        tasks: ['concat']
      }
    },

    browserSync: {
      dev: {
        bsFiles: {
          src: [
            'index.html',
            'app/views/**/*.html',
            'public/**/*'
          ]
        },
        options: {
          open: false,
          watchTask: true,
          server: './',
          rewriteRules: [{
            match: /\/static\/cmp\/search-dashboard/g,
            fn: function (req, res, match) {
                return '';
            }
        }]
        }
      },
    }
  });

  grunt.registerTask('default', ['browserSync', 'watch']);
  grunt.registerTask('build', ['sass', 'concat', 'copy']);
};
