# Content Moderation Tool

## Prerequisites
* Install node
* Install grunt globally `npm install grunt -g`
* Install bower globally `npm install bower -g` 

## Developing
* Install project dependencies `npm install` or `yarn`
* Install frontend dependencies `bower install`
* Build assets `grunt build`
* Start development server `grunt`

## Deploying to development server
* Deploy to development server from origin/develop branch `fab deploy_dev_webs -i development_server_key.key`

## Deploying to live server
* * Deploy to live server from origin/master branch `fab deploy_cmp_live -i live_server_key.key`

## Resources
* [AngularJS](https://github.com/angular/angular.js)
* [AngularUI Bootstrap](https://github.com/angular-ui/bootstrap)
* [Bootstrap](https://github.com/twbs/bootstrap-sass)
* [Style Guide](https://github.com/johnpapa/angular-styleguide)
