angular
  .module('app')
  .component('addImagesToCollection', {
    templateUrl: window.path + '/app/views/addImagesToCollection.html',
    controller: AddImagesToCollection,
    bindings: {
      images: '<',
      updateImages: '&'
    }
  });


function AddImagesToCollection($rootScope, $uibModal, api) {
  var vm = this;
  vm.showCollectionsList = false;

  vm.showCollections = function () {
    vm.showCollectionsList = true;
  };

  vm.downloadImages = function () {
    for(var i = 0; i < vm.images.length; i++) {
      window.open(vm.images[i].web_photo);
    }
  };
}