angular
  .module('app')
  .component('checkImages', {
    templateUrl: window.path + '/app/views/checkImages.html',
    controller: CheckImagesController,
    bindings: {
      images: '<',
      updateImages: '&'
    }
  });

function CheckImagesController($rootScope, $uibModal, api) {
  var vm = this;

  vm.openModal = function () {
    var modalInstance = $uibModal.open({
      component: 'checkImagesModal',
      size: 'sm'
    });

    modalInstance.result.then(function () {
      var ids = vm.images.map(function (img) {
        return img.django_id;
      });

      var params = {
        ids: ids.join(),
        actions_to_update: ['explicitly_level__safe', 'model_release__not_needed', 'status__publish'].join()
      };

      $rootScope.$broadcast('showUiBlocker');
      api.bulkUpdateContributions(params)
        .then(function (response) {
          vm.updateImages();
        })
        .catch(function (err) {
          $rootScope.$broadcast('hideUiBlocker');
        });
    });
  };
}

angular
  .module('app')
  .component('checkImagesModal', {
    templateUrl: window.path + '/app/views/checkImagesModal.html',
    controller: CheckImagesModalController,
    bindings: {
      close: '&',
      dismiss: '&'
    }
  });

function CheckImagesModalController() {
  var vm = this;

  vm.yes = function () {
    vm.close({$value: true});
  };

  vm.cancel = function () {
    vm.dismiss({$value: false});
  };
}
