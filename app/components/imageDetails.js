angular
  .module('app')
  .component('imageDetails', {
    templateUrl: window.path + '/app/views/imageDetails.html',
    controller: ImageDetailsController,
    bindings: {
      images: '<',
      filterImages: '&',
      viewImage: '&',
      tab: '<'
    }
  });

function ImageDetailsController($rootScope, $scope, api, recentCollections) {
  var vm = this;

  vm.isActionsPanelCollapsed = false;
  vm.isAddImagePanelCollapsed = false;
  vm.isDetailsPanelCollapsed = false;
  vm.newCollectionName = '';
  vm.pickitCollections = [];
  vm.userCollections = [];
  vm.recentCollections = [];
  vm.searchCollectionQuery = '';
  vm.userId = null;

  vm.imageClone = {};
  vm.imageStatus = [
    {name: 'Draft', slug: 'draft', action: 'status__draft'},
    {name: 'Pending review', slug: 'pending-review', action: 'status__pending_review'},
    {name: 'Published', slug: 'published', action: 'status__publish'},
    {name: 'Deleted', slug: 'deleted', action: 'status__delete'},
    {name: 'Unknown', slug: 'unknown', action: ''}
  ];

  vm.explicitnessLevel = [
    {name: 'Inappropriate', slug: 'inappropriate', action: 'explicitly_level__inappropriate'},
    {name: 'Allowing', slug: 'allowing', action: 'explicitly_level__allowing'},
    {name: 'Moderate', slug: 'moderate', action: 'explicitly_level__moderate'},
    {name: 'Safe', slug: 'safe', action: 'explicitly_level__safe'},
    {name: 'Unknown', slug: 'unknown', action: ''}
  ];

  vm.modelReleaseStatus = [
    {name: 'Needed', slug: 'needed', action: 'model_release__needed'},
    {name: 'Not needed', slug: 'not-needed', action: 'model_release__not_needed'},
    {name: 'Unknown', slug: 'unknown', action: ''}
  ];

  api.getPickitCollections()
  .then(function (collections) {
    vm.pickitCollections = collections;
  })
  .catch(function (err) {});

  api.getProfile()
  .then(function (user) {
    vm.userId = user.id;

    api.getAllUserCollections(vm.userId)
    .then(function (collections) {
      vm.userCollections = collections;
    })
    .catch(function (err) {});
  })
  .catch(function (err) {});

  vm.recentCollections = recentCollections.getAll() || [];

  $scope.$watch(function () {
    return vm.images;
  }, function (images) {
    if (!images) {
      return;
    }

    //Unselect all user collections
    vm.toggleActive(null);

    var imageStatus = vm.imageStatus.find(function (val) {
      return val.slug === images[0].status;
    });

    var explicitnessLevel = vm.explicitnessLevel.find(function (val) {
      return val.slug === images[0].explicitly_level;
    });

    vm.imageClone = angular.extend({}, {
      ids: images.map(function(img) {return img.django_id}),
      imageStatus: images.length === 1 ? imageStatus : null,
      explicitnessLevel: images.length === 1 ? explicitnessLevel : null,
      modelReleaseStatus: null,
      // url: images.length === 1 ? images[0].url : null,
      // user: {
      //   display_name: images.length === 1 ? images[0].user.display_name : null,
      //   url: images.length === 1 ? images[0].user.url : null
      // },
      // size: images.length === 1 ? images[0].size : null
    });

    if(images.length === 1) {
      api.getContribution(images[0].slug).then(function (contribution) {
        var releaseStatus = '';
        if (contribution.releases.length === 0) {
          releaseStatus = 'unknown';
        } else if (contribution.releases[0].status === 'not-needed') {
          releaseStatus = 'not-needed';
        } else {
          releaseStatus = 'needed';
        }

        var modelReleaseStatus = vm.modelReleaseStatus.find(function (val) {
          return val.slug === releaseStatus;
        });

        contribution.tags_admin_high = contribution.tags_admin_high && contribution.tags_admin_high.map(function(c) {
          return c.name;
        }).join(', ');

        contribution.tags = contribution.tags && contribution.tags.map(function(c) {
          return c.name;
        }).join(', ');

        contribution.tags_machine = contribution.tags_machine && contribution.tags_machine.map(function(c) {
          return c.name;
        }).join(', ');

        //Extending single image details
        vm.imageClone = angular.extend({}, contribution, vm.imageClone);

        if (vm.imageClone.modelReleaseStatus === null) {
          vm.imageClone.modelReleaseStatus = modelReleaseStatus;
        }
      })
      .catch(function(err) {
        var modelReleaseStatus = vm.modelReleaseStatus.find(function (val) {
          return val.slug === 'unknown';
        });

        if (vm.imageClone.modelReleaseStatus === null) {
          vm.imageClone.modelReleaseStatus = modelReleaseStatus;
        }
      });
     }
  });

  $scope.$watch(function () {
    return vm.imageClone.modelReleaseStatus && vm.imageClone.modelReleaseStatus.slug
  }, function(newStatus, oldStatus) {
    if (newStatus !== oldStatus) {
      if (newStatus === 'false') {
        vm.imageClone.imageStatus = vm.imageStatus[1];
      }
    }
  });

  vm.setPreset = function (preset) {
    vm.imageClone.imageStatus = vm.imageStatus[1];
    vm.imageClone.modelReleaseStatus = vm.modelReleaseStatus[0];

    switch (preset) {
      case 'moderate':
        vm.imageClone.explicitnessLevel = vm.explicitnessLevel[2];
        break;

      case 'safe':
        vm.imageClone.explicitnessLevel = vm.explicitnessLevel[3];
        break;
    }
  };

  vm.updateImage = function () {
    var actionsToUpdate = [];

    if (!vm.imageClone.imageStatus ||
        !vm.imageClone.explicitnessLevel ||
        !vm.imageClone.modelReleaseStatus ||
        vm.imageClone.imageStatus.action === '' ||
        vm.imageClone.explicitnessLevel.action === '' ||
        vm.imageClone.modelReleaseStatus.action === '') {
      return;
    }

    actionsToUpdate = [
      vm.imageClone.imageStatus.action,
      vm.imageClone.explicitnessLevel.action,
      vm.imageClone.modelReleaseStatus.action
    ].filter(function(item) { return item !== null });

    var params = {
      ids: vm.imageClone.ids.join(),
      actions_to_update: actionsToUpdate.join()
    };

    $rootScope.$broadcast('showUiBlocker');
    api.bulkUpdateContributions(params)
      .then(function () {
        $rootScope.$broadcast('hideUiBlocker');
        vm.filterImages();
      })
      .catch(function (err) {
        $rootScope.$broadcast('hideUiBlocker');
      });
  }

  vm.createCollection = function () {
    api.createCollection(vm.newCollectionName)
    .then(function (collection) {
      vm.newCollectionName = '';

      recentCollections.add(collection);
      vm.recentCollections = recentCollections.getAll();

      vm.userCollections.push(collection);
      vm.userCollections.sort(function(c1, c2) {
        return c1.title < c2.title ? -1 : c1.title > c2.title ? 1 : 0;
      });
    })
    .catch(function (err) {

    });
  }

  vm.addImagesToCollection = function () {
    var collection = vm.getActiveCollection();

    if(collection) {
      $rootScope.$broadcast('showUiBlocker');
      api.addToCollection(vm.userId, collection.slug, vm.images)
      .then(function (data) {
        vm.toggleActive(null);

        recentCollections.add(collection);
        vm.recentCollections = recentCollections.getAll();

        $rootScope.$broadcast('hideUiBlocker');
      })
      .catch(function (err) {});
    }
  };

  vm.toggleActive = function (collection, id) {
    function setActive (c) {
      if (c.id == id) {
        c.active = true;
      } else {
        c.active = false;
      }

      return c;
    }

    function setIncative (c) {
      c.active = false;
      return c;
    }

    if (collection === 'recent') {
      vm.recentCollections.map(setActive);
    } else {
      vm.recentCollections.map(setIncative);
    }

    if (collection === 'pickit') {
      vm.pickitCollections.map(setActive);
    } else {
      vm.pickitCollections.map(setIncative);
    }

    if (collection === 'user') {
      vm.userCollections.map(setActive);
    } else {
      vm.userCollections.map(setIncative);
    }
  }

  vm.downloadImages = function () {
    api.getDownloadLinks(vm.images)
    .then(function (links) {
      for(var i = 0; i < links.length; i++) {
      window.open(links[i]);
      }
    })
    .catch(function (err) {});
  };

  vm.getFilteredRecentCollections = function () {
    return vm.recentCollections.filter(function(c) {
      return c.title.toLowerCase().indexOf(vm.searchCollectionQuery.toLowerCase()) > -1;
    });
  }

  vm.getFilteredPickitCollections = function () {
    return vm.pickitCollections.filter(function(c) {
      return c.title.toLowerCase().indexOf(vm.searchCollectionQuery.toLowerCase()) > -1;
    });
  }

  vm.getFilteredUserCollections = function () {
    return vm.userCollections.filter(function(c) {
      return c.title.toLowerCase().indexOf(vm.searchCollectionQuery.toLowerCase()) > -1;
    });
  }

  vm.getActiveCollection = function () {
     var collections = vm.recentCollections.concat(vm.pickitCollections).concat(vm.userCollections).filter(function(c) {
      return c.active;
    });

     if (collections.length === 1) {
      return collections[0];
     }

     return null;
  }
}
