angular
  .module('app')
  .component('pagination', {
    templateUrl: window.path + '/app/views/pagination.html',
    controller: PaginationController,
    bindings: {
      currentPage: '=',
      itemsPerPage: '<',
      totalItems: '<',
      isDisabled: '<'
    }
  });

function PaginationController($scope) {
  var vm = this;

  vm.currentPageClone = 1;
  vm.totalPages = 1;

  vm.noPrevious = function() {
    return vm.currentPage - 1 < 1;
  }

  vm.noNext = function() {
    return vm.currentPage + 1 > Math.ceil(vm.totalItems / vm.itemsPerPage);
  }

  vm.goLeft = function() {
    if (vm.noPrevious() || vm.isDisabled) {
      return;
    }

    vm.currentPageClone = ~~vm.currentPage - 1;
    vm.currentPage = ~~vm.currentPage - 1;
  }

  vm.goRight = function() {
    if (vm.noNext() || vm.isDisabled) {
      return;
    }

    vm.currentPageClone = ~~vm.currentPage + 1;
    vm.currentPage = ~~vm.currentPage + 1;
  }

  vm.goTo = function() {
    if (!vm.currentPageClone || vm.currentPageClone < 1 || vm.currentPageClone > vm.totalPages || vm.isDisabled) {
      vm.currentPageClone = ~~vm.currentPage;

      return;
    }

    vm.currentPage = ~~vm.currentPageClone;
  }

  $scope.$watch(function() {
    return vm.currentPage;
  }, function (currentPage) {
    if (!currentPage) {
      return;
    }

    vm.currentPageClone = ~~vm.currentPage;
  });

  $scope.$watch(function () {
    return vm.totalItems;
  }, function (totalItems) {
    if (!totalItems) {
      return;
    }

    vm.totalPages = Math.ceil(vm.totalItems / vm.itemsPerPage);
  });
}