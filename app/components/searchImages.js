angular
  .module('app')
  .component('searchImages', {
    templateUrl: window.path + '/app/views/searchImages.html',
    controller: SearchImagesController,
    bindings: {
      search: '='
    }
  });

function SearchImagesController(api) {
  var vm = this;

  vm.searchString = '';

  vm.getContributors = function(query) {
    return api.getContributors(query)
     .then(function (users) {
        users.push({name: 'Search for phrase ' + query, query: query})
        
        return users;
      })
      .catch(function (err) {
        
      });
  }

  vm.selectSearchItem = function($item, $model, $label, $event) {
    vm.search = $item;
  }
}