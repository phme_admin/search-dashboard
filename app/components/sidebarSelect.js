angular.module("app")
  .component("sidebarSelect",{
    templateUrl: window.path + '/app/views/sidebarSelect.html',
     controller: SidebarSelectController,
     bindings: {
      modelRelease: '=',
      image: '=',
      explicitnessLevel: '=',
      photoCount: '<',
      getPhotosFromFirstPage: '&',
      tab: '<'
     }
});


function SidebarSelectController($rootScope, $timeout) {
    var vm = this;

    $rootScope.$on('resetStatus', resetStatus);

    vm.imageStatus = [
      {name: 'Draft', slug: 'draft', action: 'status__draft'},
      {name: 'Pending review', slug: 'pending-review', action: ''},
      {name: 'Published', slug: 'published', action: 'status__publish'},
      {name: 'Deleted', slug: 'deleted', action: 'status__delete'},
      {name: 'All', slug: 'all', action: ''},
    ];

    vm.explicitnessLevelStatus = [
      {name: 'Inappropriate', slug: 'inappropriate', action: 'explicitly_level__inappropriate'},
      {name: 'Allowing', slug: 'allowing', action: 'explicitly_level__allowing'},
      {name: 'Moderate', slug: 'moderate', action: 'explicitly_level__moderate'},
      {name: 'Safe', slug: 'safe', action: 'explicitly_level__safe'},
      {name: 'All', slug: 'all', action: ''}
    ];

    vm.modelReleaseStatus = [
      {name: 'Draft', slug: 'draft', action: ''},
      {name: 'Pending', slug: 'pending', action: ''},
      {name: 'Not trusted', slug: 'not-trusted', action: ''},
      {name: 'Needed', slug: 'needed', action: ''},
      {name: 'Not needed', slug: 'not-needed', action: ''},
      {name: 'Rejected', slug: 'rejected', action: ''},
      {name: 'Released', slug: 'released', action: ''},
      {name: 'Deleted', slug: 'deleted', action: ''},
      {name: 'Unknown', slug: 'unknown', action: ''},
      {name: 'All', slug: 'all', action: ''}
    ];

    function resetStatus() {
      vm.modelReleaseClone = vm.modelRelease;
      vm.imageClone = vm.image;
      vm.explicitnessLevelClone = vm.explicitnessLevel;
    }

    vm.updateSearch = function () {
        vm.modelRelease = vm.modelReleaseClone;
        vm.image = vm.imageClone;
        vm.explicitnessLevel = vm.explicitnessLevelClone;

        $timeout(function() {
          vm.getPhotosFromFirstPage();
        }, 0);
    }
}
