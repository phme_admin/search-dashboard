angular
  .module('app')
  .component('sortImages', {
    templateUrl: window.path + '/app/views/sortImages.html',
    controller: SortImagesController,
    bindings: {
      sortBy: '='
    }
  });

function SortImagesController() {
  var vm = this;

  vm.sortByOptions = [
    {name: 'Oldest first', slug: 'asc'},
    {name: 'Newest first', slug: 'desc'}
  ]
}
