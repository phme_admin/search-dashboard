angular
  .module('app')
  .component('uiBlocker', {
    templateUrl: window.path + '/app/views/uiBlocker.html',
    controller: UiBlockerController
  });

function UiBlockerController($rootScope) {
  var vm = this;

  vm.isActive = false;

  $rootScope.$on('showUiBlocker', function() {
    vm.isActive = true;
  });

  $rootScope.$on('hideUiBlocker', function() {
    vm.isActive = false;
  });
}
