angular
  .module('app')
  .component('viewImage', {
    templateUrl: window.path + '/app/views/viewImage.html',
    controller: ViewImageController,
    bindings: {
      show: '=',
      image: '='
    }
  });

function ViewImageController() {
  var vm = this;

  vm.close = function() {
    vm.show = false;
  };
}
