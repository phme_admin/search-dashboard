angular
  .module('app')
  .controller('HomeController', HomeController);

apiService.$inject = ['$rootScope', '$scope', 'api', '$window'];

function HomeController($rootScope, $scope, api, $window) {
  var vm = this;
  var isModifierPressed = false;

  vm.modelRelease = {name: 'All', slug: 'all', action: ''};
  vm.image = {name: 'Published', slug: 'published', action: 'status__publish'};
  vm.explicitnessLevel = {name: 'All', slug: 'all', action: ''};

  vm.photoCount = 0;

  vm.images = [];

  vm.pagination = {
    currentPage: 1,
    totalPages: 0,
    itemsPerPage: 20
  };
  vm.searchString = {};
  vm.sortBy = {name: 'Oldest first', slug: 'asc'};
  vm.loading = false;
  vm.error = false;
  vm.selectedImagesIndex = [];
  vm.selectedImages = [];
  vm.tab = 'moderation'; // moderation|curation

  $scope.$watch(function () {
    return vm.pagination.currentPage;
  }, function (newPage, oldPage) {
    if (newPage != oldPage) {
      vm.getPhotos();
    }
  });

  $scope.$watch(function () {
    return vm.sortBy;
  }, function (sortByNew, sortByOld) {
    if (sortByNew !== sortByOld) {
      vm.getPhotosFromFirstPage();
    }
  });

  $scope.$watch(function () {
    return vm.searchString;
  }, function (searchStringNew, searchStringOld) {
    if (searchStringNew !== searchStringOld) {
      vm.getPhotosFromFirstPage();
    }
  });

  $window.addEventListener('keydown', function(e) {
    var key = e.which || e.keyCode;
    if (key === 17 || key === 91) {
      isModifierPressed = true;
    }
  });

  $window.addEventListener('keyup', function(e) {
    var key = e.which || e.keyCode;
    if (key === 17 || key === 91) {
      isModifierPressed = false;
    }
  });

  vm.selectImage = function (index) {
    if (isModifierPressed) {
      vm.selectImages(index);
    } else {
      vm.selectedImagesIndex = [index];
      vm.selectedImages = [vm.images[index]];
    }
  };

  vm.selectImages = function (index) {
    if (vm.selectedImagesIndex.indexOf(index) > -1) {
      vm.selectedImagesIndex = vm.selectedImagesIndex.filter(function(i) {
        return index !== i;
      });
    } else {
      vm.selectedImagesIndex.push(index);
    }

    vm.selectedImages = vm.selectedImagesIndex.map(function(i) {
      return vm.images[i];
    });
  };

  vm.isImageSelected = function (index) {
    return vm.selectedImagesIndex.indexOf(index) > -1;
  };

  vm.filterImages = function() {
    var selectedImagesIds = vm.selectedImagesIndex.map(function(i) {
      return vm.images[i].django_id;
    });

    vm.images = vm.images.filter(function(image) {
      return selectedImagesIds.indexOf(image.django_id) < 0;
    });

    vm.selectedImagesIndex = [];
  };

  vm.selectedImagesMapper = function(i) {
    return vm.images[i];
  };

  vm.getPhotosFromFirstPage = function () {
    vm.pagination.currentPage = 1;
    vm.getPhotos();
  };

  vm.getPhotos = function () {
    var params = {
      'explicit_level': vm.tab == 'moderation' ? vm.explicitnessLevel.slug : 'all',
      'status': vm.tab == 'moderation' ? vm.image.slug : 'published',
      'query': vm.searchString.query || '',
      'by_user': vm.searchString.uri ? vm.searchString.uri.match(/[0-9]+/)[0] : '',
      'page_nr': vm.pagination.currentPage,
      'page_limit': vm.pagination.itemsPerPage,
      'sort_by': 'created_on',
      'sort_order': vm.sortBy.slug
    };

    if (vm.modelRelease.slug !== 'all' && vm.tab == 'moderation') {
      params.release_status = vm.modelRelease.slug
    }

    vm.images = [];
    vm.selectedImagesIndex = [];
    vm.viewBiggerImage = false;
    vm.loading = true;
    vm.error = false;

    api.getContributions(params)
      .then(function (data) {
        vm.images = data.data;
        vm.pagination.currentPage = ~~data.page;
        vm.photoCount = ~~data.total_count;

        vm.loading = false;
        vm.error = false;

        $rootScope.$broadcast('hideUiBlocker');
      })
      .catch(function () {
        vm.loading = false;
        vm.error = true;

        $rootScope.$broadcast('hideUiBlocker');
      });
  };

  vm.viewImage = function () {
    vm.viewBiggerImage = true;
  };

  vm.toggleSidebar = function () {
    if (!vm.sidebarOpen) {
      $rootScope.$broadcast('resetStatus');
    }

    vm.sidebarOpen = !vm.sidebarOpen;
  };

  vm.changeTab = function (tab) {
    vm.tab = tab;

    vm.getPhotos();
  };

  //Get photos on first load
  vm.getPhotos();
}