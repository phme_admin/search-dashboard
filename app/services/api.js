angular
.module('app')
.factory('api', apiService);

apiService.$inject = ['$http', '$httpParamSerializer', '$q'];

function apiService($http, $httpParamSerializer, $q) {
 var baseUrl = '/';

 return {
  getContributions: getContributions,
  getContribution: getContribution,
  bulkUpdateContributions: bulkUpdateContributions,
  getContributors: getContributors,
  getPickitCollections: getPickitCollections,
  getAllUserCollections: getAllUserCollections,
  createCollection: createCollection,
  addToCollection: addToCollection,
  getDownloadLinks: getDownloadLinks,
  getProfile: getProfile
};

function getContributions(params) {
  return $http.get(baseUrl + 'api/julietta/search/contributions/', {params: params || {}})
  .then(function (response) {
    console.info(response);
    return response.data;
  })
  .catch(function (error) {
    console.log(error);
  });
}

function getContribution(slug) {
  return $http.get(baseUrl + 'api/contributions/' + slug + '/')
  .then(function (response) {
    console.info(response);

    return response.data;
  })
  .catch(function (error) {
    console.log(error);
  });
}

function bulkUpdateContributions(params) {
  return $http.put(baseUrl + 'api/julietta/search/update/?' + $httpParamSerializer(params))
  .then(function (response) {
    console.info(response);
    return response.data;
  })
  .catch(function (error) {
    console.log(error);
  });
}

function getContributors(query) {
  return $http.get(baseUrl + 'api/search/autocomplete/', {
    params: {
      query: query,
      return_type: 'contribution'
    }})
  .then(function (response) {
    console.info(response);

    return response.data.users;
  })
  .catch(function (error) {
    console.log(error);
  });
}

function getPickitCollections() {
  return $q.all([
    $http.get(baseUrl + 'api/collections/explore/source/web/', {
      params: {
        collection_nbr: 200
      }}),
     $http.get(baseUrl + 'api/collections/explore/source/uwp/', {
      params: {
        collection_nbr: 200
      }}),
      $http.get(baseUrl + 'api/collections/explore/source/office_addin/', {
      params: {
        collection_nbr: 200
      }})
    ])
  .then(function (responses) {
    console.info(responses);

    var collections = [];
    for(var i = 0; i < responses.length; i++) {
      collections = collections
      .concat(responses[i].data.featured_collections)
      .concat(responses[i].data.hero_collections)
      .concat(responses[i].data.promoted_collections);
    }

    //Used to remove duplicates and sort by name
    return collections
    .filter(function(collection, position) {
      return collections.findIndex(function(c) {
        return c.id == collection.id;
      }) == position;
    })
    .sort(function(c1, c2) {
      return c1.title < c2.title ? -1 : c1.title > c2.title ? 1 : 0;
    });
  })
  .catch(function (error) {
    console.log(error);
  });
}

function getAllUserCollections(userId) {
  return $q.all([
    $http.get(baseUrl + 'api/users/' + userId + '/collections/', {
      params: {
        owner: 'me',
        page_limit: 200
      }}),
    $http.get(baseUrl + 'api/user/' + userId + '/collections/public/', {
      params: {
        page_limit: 200
      }})
    ])
  .then(function (responses) {
    console.info(responses);

    var collections = responses[0].data.collections.concat(responses[1].data.collections);

    //Used to remove duplicates and sort by name
    return collections
    .filter(function(collection, position) {
      return collections.findIndex(function(c) {
        return c.id == collection.id;
      }) == position;
    })
    .sort(function(c1, c2) {
      return c1.title < c2.title ? -1 : c1.title > c2.title ? 1 : 0;
    });
  })
  .catch(function (error) {
    console.log(error);
  });
}

function createCollection(title) {
    return $http({
      method: 'POST',
      url: baseUrl + 'collection/create/',
      data: {title: title},
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    })
    .then(function (response) {
      console.info(response);

      return response.data.collection;
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  function addToCollection(userId, collectionSlug, images) {
    var promises = [];

    for(var i = 0; i < images.length; i++) {
      promises.push($http({
        method: 'POST',
        url: baseUrl + 'users/' + userId + '/collection/' + collectionSlug + '/add/photo',
        data: {contribution_slug: images[i].slug},
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }));
    }

    return $q.all(promises)
    .then(function (response) {
      console.info(response);

      return response.data;
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  function getDownloadLinks(images) {
     var promises = [];

    for(var i = 0; i < images.length; i++) {
      promises.push($http.get(baseUrl + 'api/contributions/' + images[i].slug + '/download_links/'));
    }

    return $q.all(promises)
    .then(function (responses) {
      console.info(responses);

      var links = [];
      for (var i = 0; i < responses.length; i++) {
        links.push(responses[i].data.large.url);
      }

      return links;
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  function getProfile() {
    return $http.get(baseUrl + 'api/profile/')
    .then(function (response) {
      console.info(response);

      return response.data;
    })
    .catch(function (error) {
      console.log(error);
    });
  }
}
