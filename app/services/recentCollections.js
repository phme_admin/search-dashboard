angular
.module('app')
.factory('recentCollections', recentCollections);

function recentCollections () {
  return {
    add: add,
    getAll: getAll
  };

  function add (collection) {
    var collections = getAll() || [];

    var duplicateIndex = collections.findIndex(function (c) {
      return c.id === collection.id;
    });

    if (duplicateIndex > -1) {
      collections.splice(0, 0, collections.splice(duplicateIndex, 1)[0]);
    } else {
      if(collections.length >= 3) {
        collections.pop();
      }

      collections.unshift(collection);
    }

    localStorage.setItem('recentCollections', JSON.stringify(collections));
  }

  function getAll () {
    return JSON.parse(localStorage.getItem('recentCollections'));
  }
}
