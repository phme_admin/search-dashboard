# -*- coding: utf-8 -*-
from __future__ import with_statement
import getpass
from fabric.api import run, env, hosts, local, cd, task, sudo, settings, runs_once, execute
from fabric.contrib.files import exists
from fabric.utils import abort
import requests
from datetime import datetime
import time
import fpformat

dev_web_servers = [
    'phme@web-v2-de.northeurope.cloudapp.azure.com:50000',
    'phme@web-v2-de.northeurope.cloudapp.azure.com:50001'
]
dir_cmp_app = '/home/phme/pichit.me/phme_faraday/static/cmp'
env.forward_agent = True

web_servers_pickit_new = [
    'phme@web-v2-li.northeurope.cloudapp.azure.com:50000',
    'phme@web-v2-li.northeurope.cloudapp.azure.com:50001',
    'phme@web-v2-li.northeurope.cloudapp.azure.com:50002',
]
web_servers_pickit = web_servers_pickit_new

@task
@hosts(*dev_web_servers)
def deploy_dev_webs(t_start_down=None):
    """
    Deploy web servers [dev]

    :return:
    """
    if exists(dir_cmp_app):
        with cd(dir_cmp_app+'/search-dashboard'):
            run('git fetch')
            run('git checkout develop')
            run('git pull origin develop')
            run('grunt build')
            if not exists('../../../templates/cmp'):
                run('mkdir ../../../templates/cmp')
            run('cp ./index.html ../../../templates/cmp/index.html')
    else:
        run('mkdir {}'.format(dir_cmp_app))
        with cd(dir_cmp_app):
            run('git clone git@bitbucket.org:phme_admin/search-dashboard.git')
            with cd('./search-dashboard'):
                run('npm install ')
                run('sudo sudnpm install bower -g')
                run('bower install')
                run('grunt build')
                if not exists('../../../templates/cmp'):
                    run('mkdir ../../../templates/cmp')
                run('cp ./index.html ../../../templates/cmp/index.html')


@task
@hosts(*web_servers_pickit)
def deploy_cmp_live():
    if exists(dir_cmp_app):
        with cd(dir_cmp_app + '/search-dashboard'):
            run('git fetch')
            run('git checkout master')
            run('git pull origin master')
            run('grunt build')
            if not exists('../../../templates/cmp'):
                run('mkdir ../../../templates/cmp')
            run('cp ./index.html ../../../templates/cmp/index.html')

    else:
        run('mkdir {}'.format(dir_cmp_app))
        with cd(dir_cmp_app):
            run('git clone git@bitbucket.org:phme_admin/search-dashboard.git')
            with cd('./search-dashboard'):
                run('git fetch')
                run('git checkout master')
                run('git pull origin master')
                run('npm install')
                run('sudo npm install bower -g')
                run('bower install')
                run('grunt build')
